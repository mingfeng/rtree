#include "RTree.h"

RTree::RTree()
{
	_rootNode = ConstructTreeNode(NULL, NULL, 0);
}


RTree::~RTree()
{
	FreeTreeNode(_rootNode);
}


Entry* RTree::ConstructEntry(const MDimRect* rect, const DataType data)
{
	Entry* entry = new Entry();
	entry->_data = data;
	entry->_childNode = NULL;

	entry->_rect = new MDimRect;
	for (int i = 0; i < RECTDIM; i++)
	{
		entry->_rect->_min[i] = rect->_min[i];
		entry->_rect->_max[i] = rect->_max[i];
	}

	return entry;
}


Entry* RTree::ConstructEntry(TreeNode* node)
{
	Entry* entry = new Entry();
	entry->_childNode = node;
	entry->_rect = new MDimRect;
	node->_parentEntry = entry;
	AdjustRect(entry);
	return entry;
}


void RTree::DisconnectEntry(Entry* entry)
{
	delete entry->_rect;
	delete entry;
}


void RTree::DisconnectTreeNode(TreeNode* node)
{
	delete node;
}


TreeNode* RTree::ConstructTreeNode(TreeNode* parent, Entry* parentEntry, int level)
{
	TreeNode *treeNode = new TreeNode;
	treeNode->_parent = parent;
	treeNode->_parentEntry = parentEntry;
	treeNode->_level = level;
	return treeNode;
}


void RTree::FreeTreeNode(TreeNode* treeNode)
{
	for (vector<Entry*>::iterator iter = treeNode->_entries.begin(); iter != treeNode->_entries.end(); iter++)
	{
		FreeEntry(*iter);
	}
	delete treeNode;
}


void RTree::FreeEntry(Entry* entry)
{
	if (entry->_childNode != NULL)
	{
		FreeTreeNode(entry->_childNode);
	}
	delete entry->_rect;
	delete entry;
}

int RTree::Search(const MDimRect* searchRect)
{
	if (_searchResult.size() != 0)
	{
		_searchResult.clear();
	}

	return Search(_rootNode, searchRect);
}

int RTree::Search(TreeNode* node, const MDimRect* searchRect)
{
	if (node->_level > 0)
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			Search((*iter)->_childNode, searchRect);
		}
	}
	else
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			if (IsOverlap(searchRect, (*iter)->_rect))
			{
				_searchResult.push_back((*iter)->_data);
			}
		}
	}

	return _searchResult.size();
}


bool RTree::IsOverlap(const MDimRect* rect1, const MDimRect* rect2)
{
	for (int i = 0; i < RECTDIM; i++)
	{
		if (rect1->_min[i] > rect2->_max[i] || rect1->_max[i] < rect2->_min[i])
		{
			return false;
		}
	}
	return true;
}


void RTree::Insert(const MDimRect* rect, const DataType data)
{
	Entry* entry = ConstructEntry(rect, data);
	Insert(entry, 0);
}


void RTree::Insert(Entry* entry, int level)
{
	TreeNode* node = ChooseLevelNode(entry, level);
	//node->_entries.push_back(entry);
	NodeAddEntry(node, entry);

	TreeNode* newNode = NULL;
	if (node->_entries.size() > MAXENTRY)
	{
		//The node contains M+1 entries; split node;
		SplitNode(node, newNode);
	}
	AdjustTree(node, newNode);
}


TreeNode* RTree::ChooseLevelNode(Entry* entry, int level)
{
	TreeNode* node = _rootNode;

	while (node->_level != level)
	{
		CoordType rectArea = 0;
		CoordType newArea = 0;
		CoordType extraArea = 0;
		CoordType minRectArea = 0;
		CoordType minExtraArea = 0;

		TreeNode* tempNode = node;
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			rectArea = CalRectArea((*iter)->_rect);
			MDimRect* newRect = new MDimRect;
			for (int i = 0; i < RECTDIM; i++)
			{
				newRect->_min[i] = ((*iter)->_rect->_min[i] < entry->_rect->_min[i]) ? ((*iter)->_rect->_min[i]) : (entry->_rect->_min[i]);
				newRect->_max[i] = ((*iter)->_rect->_max[i] > entry->_rect->_max[i]) ? ((*iter)->_rect->_max[i]) : (entry->_rect->_max[i]);
			}
			newArea = CalRectArea(newRect);
			extraArea = newArea - rectArea;

			if (iter == node->_entries.begin())
			{
				minRectArea = rectArea;
				minExtraArea = extraArea;
				tempNode = (*iter)->_childNode;
			}
			else
			{
				if (extraArea < minExtraArea)
				{
					minExtraArea = extraArea;
					minRectArea = rectArea;
					tempNode = (*iter)->_childNode;
				}
				else if (extraArea == minExtraArea && rectArea < minRectArea)
				{
					//Choose the entry with the rectangle of smallest area if the smallest enlargenments tie
					minRectArea = rectArea;
					tempNode = (*iter)->_childNode;
				}
			}
			delete newRect;
		}
		node = tempNode;
	}
	return node;
}


TreeNode* RTree::ChooseLeaf(Entry* entry)
{
	return ChooseLevelNode(entry, 0);
}


void RTree::AdjustRect(Entry* entry)
{
	GetNodeRect(entry->_childNode, entry->_rect);
}


void RTree::NodeAddEntry(TreeNode* node, Entry* entry)
{
	if (node->_level != 0)
	{
		entry->_childNode->_parent = node;
	}
	node->_entries.push_back(entry);
}


void RTree::GetNodeRect(TreeNode* node, MDimRect* rect)
{
	for (int i = 0; i < RECTDIM; i++)
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			if (iter == node->_entries.begin())
			{
				rect->_min[i] = (*iter)->_rect->_min[i];
				rect->_max[i] = (*iter)->_rect->_max[i];
			}
			else
			{
				rect->_min[i] = (rect->_min[i] < (*iter)->_rect->_min[i]) ? (rect->_min[i]) : ((*iter)->_rect->_min[i]);
				rect->_max[i] = (rect->_max[i] > (*iter)->_rect->_max[i]) ? (rect->_max[i]) : ((*iter)->_rect->_max[i]);
			}
		}
	}
}


void RTree::ComposeRect(MDimRect* comRect, const MDimRect* rect1, const MDimRect* rect2)
{
	for (int i = 0; i < RECTDIM; i++)
	{
		comRect->_min[i] = (rect1->_min[i] < rect2->_min[i]) ? rect1->_min[i] : rect2->_min[i];
		comRect->_max[i] = (rect1->_max[i] > rect2->_max[i]) ? rect1->_max[i] : rect2->_max[i];
	}
}


void RTree::AdjustTree(TreeNode* L, TreeNode* LL)
{
	TreeNode* N = L;
	TreeNode* NN = NULL;
	if (LL != NULL)
	{
		NN = LL;
	}

	while (N != _rootNode)
	{
		TreeNode* P = N->_parent;
		Entry* E = N->_parentEntry;

		AdjustRect(E);

		if (NN != NULL)
		{
			Entry* Enn = ConstructEntry(NN);
			NodeAddEntry(P, Enn);

			if (P->_entries.size() <= MAXENTRY)
			{
				NN = NULL;
			}
			else
			{
				/*
				** P has no room to add entry, call SplitNode produce P and PP
				** Let N = P, and NN = PP
				*/
				TreeNode* PP = NULL;
				SplitNode(P, PP);
				NN = PP;
			}
		}

		N = P;
	}

	// if node split propagation caused the root to split, create a new root whose children
	// are the two resulting nodes
	if (NN != NULL && N == _rootNode)
	{
		TreeNode* newRootNode = ConstructTreeNode(NULL, NULL, N->_level+1);
		Entry* entry1 = ConstructEntry(N);
		Entry* entry2 = ConstructEntry(NN);
		NodeAddEntry(newRootNode, entry1);
		NodeAddEntry(newRootNode, entry2);
		_rootNode = newRootNode;
	}
}


void RTree::SplitNode(TreeNode* node1, TreeNode*& node2)
{
	vector<Entry*>* entries = new vector<Entry*>;
	entries->assign(node1->_entries.begin(), node1->_entries.end());
	node1->_entries.clear();

	node2 = ConstructTreeNode(NULL, NULL, node1->_level);

	Entry* entry1 = NULL;
	Entry* entry2 = NULL;
	PickSeeds(entries, entry1, entry2);
	//node1->_entries.push_back(entry1);
	//node2->_entries.push_back(entry2);
	NodeAddEntry(node1, entry1);
	NodeAddEntry(node2, entry2);

	MDimRect* nodeRect1 = new MDimRect;
	MDimRect* nodeRect2 = new MDimRect;
	MDimRect* comRect1 = new MDimRect;
	MDimRect* comRect2 = new MDimRect;

	while (entries->size() != 0)
	{
		if (node1->_entries.size() + entries->size() == MINENTRY)
		{
			for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
			{
				//node1->_entries.push_back(*iter);
				NodeAddEntry(node1, *iter);
			}
			break;
		}

		if (node2->_entries.size() + entries->size() == MINENTRY)
		{
			for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
			{
				//node2->_entries.push_back(*iter);
				NodeAddEntry(node2, *iter);
			}
			break;
		}

		Entry* nextEntry = PickNext(entries, node1, node2);

		GetNodeRect(node1, nodeRect1);
		GetNodeRect(node2, nodeRect2);

		ComposeRect(comRect1, nodeRect1, nextEntry->_rect);
		ComposeRect(comRect2, nodeRect2, nextEntry->_rect);
		
		CoordType diff1 = CalRectArea(comRect1) - CalRectArea(nodeRect1);
		CoordType diff2 = CalRectArea(comRect2) - CalRectArea(nodeRect2);

		if (diff1 > diff2)
		{
			//node2->_entries.push_back(nextEntry);
			NodeAddEntry(node2, nextEntry);
		}
		else if (diff1 < diff2)
		{
			//node1->_entries.push_back(nextEntry);
			NodeAddEntry(node1, nextEntry);
		}
		else
		{
			if (CalRectArea(nodeRect1) > CalRectArea(nodeRect2))
			{
				//node2->_entries.push_back(nextEntry);
				NodeAddEntry(node2, nextEntry);
			}
			else if (CalRectArea(nodeRect1) > CalRectArea(nodeRect2))
			{
				//node1->_entries.push_back(nextEntry);
				NodeAddEntry(node1, nextEntry);
			}
			else
			{
				if (node1->_entries.size() > node2->_entries.size())
				{
					//node2->_entries.push_back(nextEntry);
					NodeAddEntry(node2, nextEntry);
				}
				else
				{
					//node1->_entries.push_back(nextEntry);
					NodeAddEntry(node1, nextEntry);
				}
			}
		}

	}
	delete comRect1;
	delete comRect2;
	delete nodeRect1;
	delete nodeRect2;

	delete entries;
}


void RTree::PickSeeds(vector<Entry*>* entries, Entry*& entry1, Entry*& entry2)
{
	CoordType d = 0;
	MDimRect* comRect = new MDimRect;
	for (vector<Entry*>::iterator i = entries->begin(); i != entries->end(); i++)
	{
		for (vector<Entry*>::iterator j = i+1; j != entries->end(); j++)
		{
			ComposeRect(comRect, (*i)->_rect, (*j)->_rect);
			CoordType temp = CalRectArea(comRect) - CalRectArea((*i)->_rect) - CalRectArea((*j)->_rect);

			if (temp > d)
			{
				d = temp;
				entry1 = *i;
				entry2 = *j;
			}
		}
	}
	delete comRect;

	for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
	{
		if (*iter == entry1)
		{
			entries->erase(iter);
			break;
		}
	}
	for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
	{
		if (*iter == entry2)
		{
			entries->erase(iter);
			break;
		}
	}
}


Entry* RTree::PickNext(vector<Entry*>* entries, TreeNode* node1, TreeNode* node2)
{
	MDimRect* nodeRect1 = new MDimRect;
	MDimRect* nodeRect2 = new MDimRect;
	GetNodeRect(node1, nodeRect1);
	GetNodeRect(node2, nodeRect2);
	CoordType nodeRectArea1 = CalRectArea(nodeRect1);
	CoordType nodeRectArea2 = CalRectArea(nodeRect2);
	
	MDimRect* comRect1 = new MDimRect;
	MDimRect* comRect2 = new MDimRect;
	CoordType diff = 0;
	CoordType largediff = 0;
	Entry* nextEntry = NULL;
	for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
	{
		ComposeRect(comRect1, nodeRect1, (*iter)->_rect);
		ComposeRect(comRect2, nodeRect2, (*iter)->_rect);
		CoordType d1 = CalRectArea(comRect1)-CalRectArea(nodeRect1);
		CoordType d2 = CalRectArea(comRect2)-CalRectArea(nodeRect2);
		if (d1>d2)
		{
			diff = d1-d2;
		}
		else
		{
			diff = d2-d1;
		}

		if (diff > largediff)
		{
			largediff = diff;
			nextEntry = *iter;
		}
	}

	delete comRect1;
	delete comRect2;
	delete nodeRect1;
	delete nodeRect2;

	for (vector<Entry*>::iterator iter = entries->begin(); iter != entries->end(); iter++)
	{
		if (*iter == nextEntry)
		{
			entries->erase(iter);
			break;
		}
	}

	return nextEntry;
}


void RTree::Delete(const MDimRect* rect, const DataType data)
{
	Delete(_rootNode, rect, data);
}

void RTree::Delete(TreeNode* node, const MDimRect* rect, const DataType data)
{
	TreeNode* L = NULL;
	FindLeaf(node, rect, data, L);
	if (L == NULL)
	{
		return;
	}
	for (vector<Entry*>::iterator iter = L->_entries.begin(); iter != L->_entries.end(); iter++)
	{
		if ((*iter)->_data == data)
		{
			L->_entries.erase(iter);
			break;
		}
	}

	CondenseTree(L);

	if (_rootNode->_entries.size() == 1)
	{
		TreeNode* temp = _rootNode;
		_rootNode = _rootNode->_entries[0]->_childNode;
		_rootNode->_parent = NULL;
		_rootNode->_parentEntry = NULL;
		temp->_entries.clear();
		DisconnectTreeNode(temp);
	}
}


void RTree::FindLeaf(TreeNode* node, const MDimRect* rect, const DataType data, TreeNode*& resultNode)
{
	if (node->_level != 0)
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			if (IsOverlap((*iter)->_rect, rect))
			{
				FindLeaf((*iter)->_childNode, rect, data, resultNode);
			}
		}
	}
	else
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			if ((*iter)->_data == data)
			{
				resultNode = node;
			}
		}
	}
}


void RTree::CondenseTree(TreeNode* node)
{
	TreeNode* N = node;
	vector<TreeNode*> elimNodes;
	
	while (N != _rootNode)
	{
		TreeNode* P = N->_parent;
		Entry* E = N->_parentEntry;
		if (N->_entries.size() < MINENTRY)
		{
			for (vector<Entry*>::iterator iter = P->_entries.begin(); iter != P->_entries.end(); iter++)
			{
				if (*iter == E)
				{
					DisconnectEntry(*iter);
					P->_entries.erase(iter);
					elimNodes.push_back(N);
					break;
				}
			}
		}
		else
		{
			AdjustRect(E);
		}
		N = P;
	}

	for (vector<TreeNode*>::iterator iNode = elimNodes.begin(); iNode != elimNodes.end(); iNode++)
	{
		for (vector<Entry*>::iterator iEntry = (*iNode)->_entries.begin(); iEntry != (*iNode)->_entries.end(); iEntry++)
		{
			Insert(*iEntry, (*iNode)->_level);
		}
	}
}

void RTree::FilePrintTree(char* outFile)
{
	if (_rootNode == NULL)
	{
		return;
	}

	ofstream fout(outFile);
	
	PrintNode(_rootNode, fout);

	fout.close();

}

void RTree::PrintNode(TreeNode* node, ofstream& fout)
{
	if (node == NULL)
	{
		return;
	}
	
	for (int i = 0; i < _rootNode->_level - node->_level; i++)
	{
		fout<<"\t";
	}

	fout<<"[";
	for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
	{
		fout<<"("<<(*iter)->_rect->_min[0]<<","<<(*iter)->_rect->_min[1]<<","<<(*iter)->_rect->_max[0]<<","<<(*iter)->_rect->_max[1]<<"|";
		if (node->_level != 0)
		{
			fout<<"*)";
		}
		else
		{
			fout<<(*iter)->_data<<")";
		}
	}
	fout<<"]"<<endl;

	if (node->_level != 0)
	{
		for (vector<Entry*>::iterator iter = node->_entries.begin(); iter != node->_entries.end(); iter++)
		{
			PrintNode((*iter)->_childNode, fout);
		}
	}
}
 

void RTree::FileReadTree(char* inFile)
{
	ifstream fin(inFile);

	MDimRect* oneRect = new MDimRect;
	long id;

	while (!fin.eof())
	{
		fin>>id>>oneRect->_min[0]>>oneRect->_min[1]>>oneRect->_max[0]>>oneRect->_max[1];

		Insert(oneRect, id);
	}

	delete oneRect;
	fin.close();
}

void RTree::PrintSearchResult(char* outFile)
{
	ofstream fout(outFile);

	for (vector<DataType>::iterator iter = _searchResult.begin(); iter != _searchResult.end(); iter++)
	{
		fout<<*iter<<endl;
	}

	fout.close();
}
