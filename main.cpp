#include "RTree.h"

int main()
{
	RTree* tree = new RTree();

	tree->FileReadTree("intree.txt");
	tree->FilePrintTree("outtree1.txt");

	MDimRect* deleteRect1 = new MDimRect;
	deleteRect1->_min[0] = 2;
	deleteRect1->_min[1] = 3;
	deleteRect1->_max[0] = 5;
	deleteRect1->_max[1] = 7;

	MDimRect* deleteRect2 = new MDimRect;
	deleteRect2->_min[0] = 2;
	deleteRect2->_min[1] = 4;
	deleteRect2->_max[0] = 6;
	deleteRect2->_max[1] = 7;

	tree->Delete(deleteRect1, 100010);

	tree->FilePrintTree("outtree2.txt");

	tree->Delete(deleteRect2, 100004);

	tree->FilePrintTree("outtree3.txt");

	delete deleteRect1;
	delete deleteRect2;

	
	MDimRect* searchRect = new MDimRect;
	searchRect->_min[0] = 1;
	searchRect->_min[1] = 1;
	searchRect->_max[0] = 2;
	searchRect->_max[1] = 2;

	tree->Search(searchRect);
	tree->PrintSearchResult("searchresult.txt");

	delete searchRect;

	delete tree;

	return 0;
}