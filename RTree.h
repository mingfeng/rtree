#ifndef RTREE_H
#define RTREE_H

/*
** Description: 
**		A C++ version of RTree structure
** Author: 
**		Yu Mingfeng
** Reference: 
**		[1]Antonin Guttman. R-Tree: A Dynamic Index Structure for Spatial Searching. 1984
*/

/*
** Macros:
** MAXENTRY: the max number of entries in a tree node
** MINENTRY: the min number of entries in a tree node
** RECTDIM: the dimention os the spatial data objects, the structure and the algorithm also support multi-dimensional data
*/
#define MAXENTRY 4
#define MINENTRY MAXENTRY/2
#define RECTDIM 2

#include <fstream>
#include <vector>
using namespace std;

typedef long DataType;		//The data type of data identifier, for easy change according to data
typedef double CoordType;	//The coordinate type of the spatial data, for easy change according to data

typedef struct _MDimRect MDimRect;
typedef struct _Entry Entry;
typedef struct _TreeNode TreeNode;

struct _MDimRect
{
	CoordType _min[RECTDIM];	//Min values of bounding box
	CoordType _max[RECTDIM];	//Max values of bounding box
};

struct _Entry
{
	MDimRect* _rect;		//Bounding box of the entry
	TreeNode* _childNode;	//If the entry is in a non-leaf node, it has the pointer to a child node
	DataType _data;			//If the entry is in leaf node, it stores the identifier of the data
};

struct _TreeNode
{
	int _level;					//The level of tree node, 0 in leaf node, positive integer in a non-leaf node
	TreeNode* _parent;			//The pointer to its parent node
	Entry* _parentEntry;		//The pointer to its parent entry
	vector<Entry*> _entries;    //The enetries the node stores
};


class RTree
{
public:
	RTree();
	virtual ~RTree();

public:
	/* 
	** Search in the tree or sub-tree the spatial data objects that overlap the given search rect, the result will be stored in the _searchResult
	** 
	** Para:
	** node: start searching node, if not given start from the root node
	** searchRect: the range search rectangle
	**
	** Return value: the number of search result
	*/
	int Search(const MDimRect* searchRect);
	int Search(TreeNode* node, const MDimRect* searchRect);

	/*
	** Insert a spatial data object to a leaf node - level 0. Need to firstly get the MBR of the spatial data object
	**
	** Para:
	** rect: the MBR of the spatial data object
	** data: the identifier of the spatial data object
	*/
	void Insert(const MDimRect* rect, const DataType data);

	/*
	** Insert an entry to the tree. It could be either a data entry or an entry with child node
	** 
	** Para:
	** entry: the entry need to be insert
	** level: the level the entry should be in
	*/
	void Insert(Entry* entry, int level);

	/*
	** Delete an entry from the tree
	** 
	** Para:
	** node: the sub-tree specified by the node in which the deletion happens, if not given, the whole tree
	** rect: the MBR of the data object
	** data: the identifier of the data
	*/
	void Delete(const MDimRect* rect, const DataType data);
	void Delete(TreeNode* node, const MDimRect* rect, const DataType data);

	/*
	** Clear the SearchResult
	*/
	void ClearSearchResult()
	{
		_searchResult.clear();
	}

	vector<DataType> GetSearchResult()
	{
		return _searchResult;
	}
protected:
	/*
	** Select a node to insert the entry, the leaf node is in level 0
	*/
	TreeNode* ChooseLevelNode(Entry* entry, int level);
	TreeNode* ChooseLeaf(Entry* entry);

	/*
	** Ascend from a leaf node to the root, adjusting covering covering rectangles and propagating node split as necessary
	** Called by Insert
	*/
	void AdjustTree(TreeNode* L, TreeNode* LL);

	/*
	** When the node stores more than MAXENTRY entries, the node need to be split into two node
	*/
	void SplitNode(TreeNode* node, TreeNode*& newNode);

	/*
	** Select two seeds into two different groups, and these two entries will be deleted from the container
	*/
	void PickSeeds(vector<Entry*>* entries, Entry*& entry1, Entry*& entry2);

	/*
	** Select a entry from the container, the entry will be deleted
	*/
	Entry* PickNext(vector<Entry*>* entries, TreeNode* node1, TreeNode* node2);

	/*
	** Calculate the area (volumn if multi-dimentional rect) of the rect
	*/
	inline CoordType CalRectArea(const MDimRect* rect)
	{
		CoordType area = 1;
		for (int i = 0; i < RECTDIM; i++)
		{
			area *= rect->_max[i]-rect->_min[i];
		}

		return area;
	}

	void FindLeaf(TreeNode* node, const MDimRect* rect, const DataType data, TreeNode*& resultNode);
	void CondenseTree(TreeNode* node);

protected:
	TreeNode* _rootNode;
	vector<DataType> _searchResult;	

protected:
	/*
	** Construct an entry for a leaf node;
	*/
	Entry* ConstructEntry(const MDimRect* rect, const DataType data);
	/*
	** Construct an entry for a non-leaf node;
	*/
	Entry* ConstructEntry(TreeNode* node);

	TreeNode* ConstructTreeNode(TreeNode* parent, Entry* parentEntry, int level);

	/*
	** This two functions free all the treenodes and entries recursively.
	** called only need to destroy the R tree.
	*/
	void FreeEntry(Entry* entry);
	void FreeTreeNode(TreeNode* treeNode);

	/*
	**These two functions simply delete the memory that the pointer point at
	*/
	void DisconnectEntry(Entry* entry);
	void DisconnectTreeNode(TreeNode* treeNode);

	void NodeAddEntry(TreeNode* node, Entry* entry);

	/*
	** Adjust the rectagnle so that it tightly encloses all entry rectangles in entry's childnode
	*/
	void AdjustRect(Entry* entry);

	/*
	** Obtain the rectagle covering current entries in a node
	*/
	void GetNodeRect(TreeNode* node, MDimRect* rect);

	void ComposeRect(MDimRect* comRect, const MDimRect* rect1, const MDimRect* rect2);

	/*
	** Judge if two rects overlap
	*/
	bool IsOverlap(const MDimRect* rect1, const MDimRect* rect2);

public:
	/*
	** Input and output, just for self test
	*/
	void FilePrintTree(char* outFile);
	void PrintNode(TreeNode* node, ofstream& fout);
	void PrintEntry(Entry* entry, ofstream& fout);
	void FileReadTree(char* inFile);
	void PrintSearchResult(char* outFile);
};

#endif